import React, { Component } from "react";
import Grid from "@material-ui/core/Grid";
import "./App.css";
// logo
import ptLogo from "./images/pt_logo.png";
import proptiger from "./images/proptiger.png";
import googlePlay from "./images/google-play.png";
import appStore from "./images/app-store.png";
// icon
import MailOutlineIcon from "@material-ui/icons/MailOutline";
import PhoneAndroidIcon from "@material-ui/icons/PhoneAndroid";
import RoomIcon from "@material-ui/icons/Room";
import { Button } from "@material-ui/core";

class App extends Component {
  render() {
    return (
      <Grid
        container
        direction="column"
        justify="center"
        alignItems="center"
        className="firstColumnItemParent"
      >
        <Grid
          item
          className="firstColumnItem"
          xs={12}
          sm={12}
          md={12}
          lg={12}
          xl={12}
        >
          <Grid
            container
            direction="column"
            justify="center"
            alignItems="center"
          >
            <Grid
              item
              className="secondColumnItem"
              xs={12}
              sm={12}
              md={4}
              lg={4}
              xl={4}
            >
              <Grid
                container
                direction="row"
                justify="flex-start"
                alignItems="center"
              >
                <Grid
                  item
                  className="secondColumnItem1stRow1stItem"
                  xs={12}
                  sm={12}
                  md={12}
                  lg={12}
                  xl={12}
                >
                  <div className="logo">
                    <img src={ptLogo} alt="" />
                  </div>
                </Grid>
                <Grid
                  item
                  className="secondColumnItem1stRow2ndItem"
                  xs={12}
                  sm={12}
                  md={12}
                  lg={12}
                  xl={12}
                >
                  <h2 className="name">Krunal Joshi</h2>
                  <h3 className="destination">Senior Team Manager</h3>
                  <div className="businessImages">
                    <div className="businessImagesInner">
                      <a href="https://www.proptiger.com/" target="_blank">
                        <img src={proptiger} alt="proptiger" />
                      </a>
                    </div>
                    <div className="stores">
                      <a
                        href="https://play.google.com/store/apps/details?id=com.proptiger&hl=en_IN"
                        target="_blank"
                      >
                        <img src={googlePlay} alt="google-play" />
                      </a>
                      <a
                        href="https://apps.apple.com/us/app/proptiger-real-estate-property/id935244607"
                        target="_blank"
                      >
                        <img src={appStore} alt="app-store" />
                      </a>
                    </div>
                  </div>
                  <div className="socialParent">
                    <div className="socialListItem">
                      <MailOutlineIcon />
                      <a href="mailto:krunal.joshi@proptiger.com">
                        krunal.joshi@proptiger.com dgsgfg
                      </a>
                    </div>
                    <div className="socialListItem">
                      <PhoneAndroidIcon />
                      <a href="tel:9766362632">9766362632</a>
                    </div>
                    <div className="socialListItem">
                      <RoomIcon />
                      <span>
                        KBC 6th Floor, Bund Garden Sangamvadi, Pune - 411001
                      </span>
                    </div>
                  </div>
                  <div className="buttons">
                    <a
                      href="https://api.procontact.app/images/vcf/Krunal_1927.vcf"
                      target="_blank"
                    >
                      <Button variant="contained">Download</Button>
                    </a>
                    <a>
                      <Button variant="contained">Share</Button>
                    </a>
                  </div>
                </Grid>
              </Grid>
            </Grid>

            <Grid
              item
              className="thirdColumnItem"
              xs={12}
              sm={12}
              md={12}
              lg={12}
              xl={12}
            >
              <p className="proContact">
                Powered by{" "}
                <a href="https://procontact.app/" target="_blank">
                  ProContact.app
                </a>
              </p>
            </Grid>
          </Grid>
        </Grid>
      </Grid>
    );
  }
}

export default App;
